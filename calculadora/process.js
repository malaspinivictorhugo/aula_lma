const {createApp} = Vue;
createApp
({
    data()    {

        return{
            display:"0",
            operandoAtual:null,
            operandoAnterior:null,
            operador:null,
        }//Fechamento do return
    },//Fechamento de Função "data"
    methods:{
        handleButtonClick(botao){
            switch(botao){
                case "+":
                case "-":
                case "/":
                case "x":
                    this.handleOperation(botao);
                    break;
                case ".":
                    this.handleDecimal();
                    break;
                case "=":
                    this.handleEquals(botao);
                    break
                default:
                    this.handleNumber(botao);
                    break;

            }
        },//Fechamento handeButtonClick
        handleNumber(numero){
                if(this.display === "0")
                {
                    this.display = numero.toString();
                }
                else
                {
                    this.display += numero.toString();  
                }
        },//Fechamento do handleNumber
        
    handleOperation(operacao){
        if(this.operandoAtual !== null){
            this.handleEquals();
        }//Fechamneto do if
        this.operador = operacao;

        this.operandoAtual = parseFloat(this.display);
            this.display = "0";
            
    
},//Fechamento handOperation

    handleEquals(){
        const displayAtual = parseFloat(this.display);
        if(this.operandoAtual !== null && this.operador !== null){
            switch(this.operador){
                case"+":
                this.display = (this.operandoAtual + displayAtual). toString();
                break;
                case"-":
                this.display = (this.operandoAtual - displayAtual). toString();
                break;
                case"/":
                this.display = (this.operandoAtual / displayAtual). toString();
                break;
                case"x":
                this.display = (this.operandoAtual * displayAtual). toString();
                break;
            }//Fim do switch
            this.operandoAnterior = this.operandoAtual;
            this.operandoAtual = null;
            this.operador = null;

        }//Fechamento do if
        else
        {
            this.operandoAnterior = displayAtual;   

        }//fechamneto do else
    },//Fechamneto do Equals

    handleDecimal(){
        if(this.display.includes)
        {
            this.display += ".";


        }//Fechamneto do if
    }, //Fechamento do handleDecimal
    
    handleClear()
    {
        this.display = "0";
        this.operandoAtual = null;
        this.operandoAnterior = null;
        this.operador = null;
    }
    
},/*fim do methods*/

}).mount("#app"); ////Fechamento do "createApp"