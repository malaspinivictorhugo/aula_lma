const {createApp} = Vue;
createApp ({
    data(){
        return {
            randomIndexIternet: 0,
            randomIndex: 0,

            //vetor de imagem locais
            imagemLocais:[
                './Imagens/lua.jpg',
                './Imagens/SENAI_logo.png',
                './Imagens/sol.jpg'    
            ],
            imagensInternet:[
                'https://d2bsjm9patfdz0.cloudfront.net/images/0000397_dora-aventureira.jpeg',
                'https://img.freepik.com/vetores-premium/menino-feliz-e-fofo-andando-de-bicicleta_97632-4426.jpg?w=2000',
                'https://observatoriodatv.uol.com.br/wp-content/uploads/2016/02/Conhe%C3%A7a-o-respons%C3%A1vel-pelo-Dollynho-que-sumiu-dos-comerciais-do-refrigerente-e-gerou-revolta-na-web.jpg'
            ],
        };//fim do return
    },//fim da data 

    computed:{
        randomImage()
        {
            console.log(this.randomIndex);
            return this.imagemLocais[this.randomIndex];
        },//fim do randomImage
        randomImageNet()
        {
            return this.imagensInternet[this.randomIndexIternet];

        },//fim do randomImage
    },//fim do computed
    methods: {
        getRandomimage()
        { 
            this.randomIndex = Math.floor(Math.random() * this.imagemLocais.length);

            this.randomIndexIternet = Math.floor(Math.random() * this.imagensInternet.length);

        },
    },//fim do methods

}).mount("#app");